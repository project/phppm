We use Drupal primarily as a platform for application development.  This
includes the creation of Drupal modules, but seldom "content" in the
Drupal sense.

A side effect of our development is that we use a lot of 3rd party PHP
code that has been put in the public domain.  A lot of this code is shared
between the modules we developed.  Which leads to a major headache when we
extend/modify the code or fixes are provided by the actual developers, i.e.,
we have to update many (more than one anyway) modules with the new code.

It would be better if we could factor the 3rd party code into a single place
and manage it and updates to it independently.

There is a similar solution extant for Drupal for Javascript/CSS, the jQuery
Plugin Handler (jQp) but it doesn't also manage PHP code.

So, I grabbed a copy of jQp and hacked it so that it ALSO manages PHP code.

This module is the result.
